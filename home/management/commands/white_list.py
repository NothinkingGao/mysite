# 导入白名单命令
from django.core.management.base import BaseCommand, CommandError
import xlrd
from home.models import *


class Command(BaseCommand):

    def handle(self, *args, **options):

        base_url = "/Users/gaoming/dockers/mysite/uploads/"
        path = "{0}/white_list.xlsx".format(base_url)
        try:
            data = xlrd.open_workbook(path)
            sheet = data.sheets()[0]
        except Exception as e:
            print(e)
            print("file not exists!")

        regions = set()
        for index in range(1, sheet.nrows):
            row = sheet.row_values(index)
            if row:
                try:
                    # 解析数据,保存到相应的流量表当中
                    ip, ip_type = row[0], row[1]

                    # regions.add(location_name)
                    WhiteList.objects.create(ip=ip, ip_type=ip_type).save()
                    print(ip, ip_type)
                    # ServiceTag.objects.get_or_create(
                    #     service_id=service_id,
                    #     tag_id=tag_id
                    # )
                except Exception as e:
                    print(e)

        # for name in regions:
        #     Location.objects.create(name=name).save()

        print("导入流量池数据成功!")
