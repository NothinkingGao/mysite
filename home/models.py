from django.db import models

# Create your models here.


class Device(models.Model):

    class Meta:
        verbose_name = '设备表'

    device_name = models.CharField(max_length=512)
    company = models.CharField(u'Vendor', max_length=128)
    login_ip = models.CharField(max_length=64)
    device_user = models.CharField(u'Username', max_length=128)
    password = models.CharField(max_length=128)
    loopback_ip = models.CharField(max_length=64)
    flows = models.FileField(upload_to='uploads/', verbose_name="Flow文件", null=True)

    def __str__(self):
        return self.device_name


class FlowInfoMation(models.Model):

    class Meta:
        verbose_name = '流量表'

    device = models.ForeignKey('Device', on_delete=models.CASCADE)
    source_ip = models.CharField(max_length=64)
    des_ip = models.CharField(max_length=64)
    next_hop = models.CharField(max_length=64)
    speed = models.CharField(max_length=64)


class Location(models.Model):

    class Meta:
        verbose_name = "地区"
        verbose_name_plural = "地区"

    name = models.CharField(max_length=32, verbose_name="地区")

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class AddressPool(models.Model):

    class Meta:
        verbose_name = '地址池'

    des_ip = models.CharField(max_length=64)
    location = models.ForeignKey('Location', on_delete=models.CASCADE)


class WhiteList(models.Model):

    class Meta:
        verbose_name = '白名单'

    TYPE_CHOICES = (
        ('DENY-SOURCE', 'DENY-SOURCE'),
        ('DESTINATION', 'DESTINATION'),
        ('NEXT-HOP', 'NEXT-HOP')
    )
    ip = models.CharField(max_length=64)
    ip_type = models.CharField(max_length=64, choices=TYPE_CHOICES)
