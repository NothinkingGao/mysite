from django.contrib import admin
import xlrd
import os
from .models import Device, FlowInfoMation, AddressPool, WhiteList, Location


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('device_name', 'company', 'device_user', 'password')

    def save_model(self, request, obj, form, change):
        # 保存时删除原数据、解析excel文件到flow表中
        obj.user = request.user

        # 先保存文件
        super().save_model(request, obj, form, change)

        # 删除原数据
        FlowInfoMation.objects.filter(device=obj).delete()

        # 获得上传的文件
        if form.cleaned_data['flows'] is not None:
            flows = form.cleaned_data['flows']
            try:
                data = xlrd.open_workbook(obj.flows.path)
                sheet = data.sheets()[0]
            except Exception as e:
                print(e)
                print("file not exists!")

            for index in range(1, sheet.nrows):
                row = sheet.row_values(index)
                if row:
                    try:
                        # 导入新数据
                        data = {
                            "device": obj,
                            "source_ip": row[0],
                            "des_ip": row[1],
                            "next_hop": row[2],
                            "speed": row[3]
                        }
                        FlowInfoMation.objects.create(**data).save()
                        print("导入流量池数据成功!")
                    except Exception as e:
                        print("wrong action:service_id:{0},tag_id:{1}".format(service_id, tag_id))
                        print(e)


class FlowInfoMationAdmin(admin.ModelAdmin):
    list_display = ('device', 'source_ip', 'des_ip', 'next_hop', 'speed')
    list_filter = ('device',)


class AddressPoolAdmin(admin.ModelAdmin):
    search_fields = ('location',)
    list_filter = ('location',)
    list_display = ('des_ip', 'location')


class LocationAddressAdmin(admin.ModelAdmin):
    list_display = ('name',)


class WhiteListAdmin(admin.ModelAdmin):
    list_display = ('ip', 'ip_type')
    list_filter = ('ip_type',)


admin.site.register(Device, DeviceAdmin)
admin.site.register(FlowInfoMation, FlowInfoMationAdmin)
admin.site.register(AddressPool, AddressPoolAdmin)
admin.site.register(WhiteList, WhiteListAdmin)
admin.site.register(Location, LocationAddressAdmin)
