
from django.template.response import TemplateResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
import random
from .models import Device, FlowInfoMation, AddressPool, WhiteList, Location
import paramiko
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import IPy

# Create your views here.


def pool(request):
    '''地址池'''
    device_id = int(request.GET.get("device_id"))
    device = Device.objects.get(id=device_id)

    locations = Location.objects.all()
    pools = AddressPool.objects.all()

    # loopback地址,就是除了自身外其他设备
    loopbacks = Device.objects.exclude(id=device_id)

    # 默认数量
    default_count = 5

    context = {
        "device": device,
        "default_count": default_count,
        "locations": locations,
        "loopbacks": loopbacks,
        "pools": pools,
    }

    return render(request, 'home/pool.html', context)


@csrf_exempt
def pool_optimize(request):
    # 地址池优化方法
    print(request.method)
    if request.method == "POST":
        print(request.POST.dict())

        device_id = request.POST.get("device_id")
        # 跳转地址
        loopback_ip = request.POST.get("loopback_ip")
        loopback = Device.objects.get(loopback_ip=loopback_ip)

        location_id = request.POST.get("location_id")
        count = int(request.POST.get("count", 5))

        locations = AddressPool.objects.filter(location=location_id)

        total_count = locations.count()

        count = count if count <= total_count else total_count
        choices = random.sample(list(locations), count)

        # 获得设备
        device = Device.objects.get(id=device_id)

        ip_list = [item.des_ip for item in choices]

        # 生成的命令
        commands = get_commands(device, ip_list, loopback_ip)

        context = {
            "device": device,
            "ip_list": ip_list,
            "ips": ",".join(ip_list),
            "commands": commands,
            "loopback_ip": loopback_ip,
            "loopback": loopback
        }

        return TemplateResponse(request, "home/source_ips.html", context)

    return JsonResponse({"result": "not allowed method!"})


def device(request):
    '''设备列表'''
    devices = Device.objects.all()
    return TemplateResponse(request, 'home/device.html', {'devices': devices})


def flow(request):
    # 流量池,与设备一一对应
    device_id = request.GET.get("device_id")
    device = Device.objects.get(id=device_id)

    # loopback地址,就是除了自身外其他设备
    loopbacks = Device.objects.exclude(id=device_id)
    print(loopbacks)

    flows = FlowInfoMation.objects.filter(device_id=device_id)

    results = filter_ips(flows)

    context = {
        "device": device,
        "loopbacks": loopbacks,
        "flows": results
    }
    return TemplateResponse(request, 'home/flow.html', context)


def filter_ips(flows):
    '''
        白名单过滤规则
    '''
    whitelists = WhiteList.objects.all()

    # 取到三种地址
    source_ips = [item.ip for item in whitelists.filter(ip_type="DENY-SOURCE")]
    destination = [item.ip for item in whitelists.filter(ip_type="DESTINATION")]
    nextloop = [item.ip for item in whitelists.filter(ip_type="NEXT-HOP")]

    results = flows.exclude(source_ip__in=source_ips).filter(next_hop__in=nextloop).order_by("-speed")

    # 通过IPy过滤destination地址
    final_results = []
    for dest in destination:
        for result in results:
            try:
                IP = IPy.IP(dest)
                if IP.overlaps(result.des_ip):
                    final_results.append(result)
            except Exception as e:
                pass
    return final_results


@csrf_exempt
def flow_optimize(request):
    # 流量池优化方法
    if request.method == "POST":
        device_id = request.POST.get("device_id")

        print(request.POST.dict())
        loopback_ip = request.POST.get("loopback_ip")

        # 设备
        device = Device.objects.get(id=device_id)
        loopback = Device.objects.get(loopback_ip=loopback_ip)

        # 过滤
        flows = FlowInfoMation.objects.filter(device=device)
        results = filter_ips(flows)

        # 拿到destination的ip,显示前20,然后再跑命令
        ip_list = [item.des_ip for item in results]

        # 生成的命令
        commands = get_commands(device, ip_list, "")

        context = {
            "device": device,
            "ip_list": ip_list,
            "ips": ",".join(ip_list),
            "commands": commands,
            "loopback_ip": loopback_ip,
            "loopback": loopback
        }

        return TemplateResponse(request, "home/source_ips.html", context)
    return JsonResponse({"result": "not allowed method!"})


def get_commands(device, ip_list, loopback_ip):
    '''
            根据设备类型生成命令行
    '''
    commands = []
    if device.company.lower() == "huawei":
        commands.append("system-view")
        for ip in ip_list:
            commands.append("ip route-static {0} {1}".format(ip.replace("/", " "), loopback_ip))
        commands.append("commit")
    else:
        commands.append("configure")
        for ip in ip_list:
            commands.append("set route-options static route {0} next-hop {1} resolve".format(ip, loopback_ip))
        commands.append("commit")

    return commands


@csrf_exempt
def generate_command(request):
    # 生成命令并执行
    if request.method == "POST":
        device_id = request.POST.get("device_id")
        ip_list = request.POST.get("ip_list")
        loopback_ip = request.POST.get("loopback_ip")

        # 生成命令
        device = Device.objects.get(id=device_id)
        commands = get_commands(device, ip_list, loopback_ip)

        # 执行命令
        return send_command(request, device, commands)

    return JsonResponse({"result": "not allowed method!"})


def send_command(request, device, commands):
    # 命令下发
    hostname = device.login_ip
    port = "22"
    username = device.device_user
    password = device.password

    message = "成功优化!"
    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.WarningPolicy())

        client.connect(hostname, port, username, password)
        chan = client.invoke_shell()

        print("*** Connecting...")
        for command in commands:
            stdin, stdout, stderr = client.exec_command(command)
            for line in stdout:
                print('... ' + line.strip('\n'))
    except OSError:
        message = "无法连接设备!"
    finally:
        client.close()

    return render(request, 'home/result.html', {"message": message})
